use std::sync::Arc;

use crate::engine::generator::{Generator, MusicGenerator, Note};
use crate::engine::instruments::Instrument;
use crate::engine::notes::*;
use crate::engine::utils::types::Second;

pub struct TestGenerator;

impl MusicGenerator for TestGenerator {
    fn new_generator(tempo: Option<u32>) -> Generator {
        let tempo = tempo.unwrap_or(60);
        let mut generator = Generator::new_generator(
            0,
            tempo,
            Vec::from([
                Arc::new(Instrument::new_tone("tone")),
                Arc::new(Instrument::new_crash("crash")),
                Arc::new(Instrument::new_bell("bell")),
                Arc::new(Instrument::new_supersaw("supersaw")),
                Arc::new(Instrument::new_snare("snare")),
                Arc::new(Instrument::new_kick("kick")),
            ]),
        );
        generator.time_signature = generator.instruments.len() as u32;
        generator.gen_notes = TestGenerator::gen_notes;
        generator
    }

    fn gen_notes(gen: &mut Generator) -> Second {
        let beat_length = gen.beat_length();

        let measure_length = beat_length * gen.time_signature as f32;

        eprintln!("New measure (length: {})", measure_length);

        for (beat, beat_timestamp) in gen.measure(gen.time_signature as usize) {
            gen.notes.push_back(Note {
                instrument: gen.instruments[beat].clone(),
                start_timestamp: beat_timestamp,
                frequency: C / 4.0,
                duration: beat_length * 3.0 / 4.0,
            });
        }

        gen.print_notes();
        measure_length
    }
}
