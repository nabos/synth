use std::sync::Arc;

use crate::engine::generator::{Generator, MusicGenerator, Note};
use crate::engine::instruments::Instrument;
use crate::engine::notes::*;
use crate::engine::utils::types::Second;
use rand::seq::SliceRandom;
use rand::thread_rng;

pub struct LofiGenerator;

impl MusicGenerator for LofiGenerator {
    fn new_generator(tempo: Option<u32>) -> Generator {
        let tempo = tempo.unwrap_or(40);
        let mut gen = Generator::new_generator(
            0,
            tempo,
            Vec::from([
                Arc::new(Instrument::new_flute("flute")),
                Arc::new(Instrument::new_supersaw("saw")),
                Arc::new(Instrument::new_snare("snare")),
                Arc::new(Instrument::new_kick("kick")),
            ]),
        );
        gen.gen_notes = LofiGenerator::gen_notes;
        gen
    }

    fn gen_notes(gen: &mut Generator) -> Second {
        let fundamental = *[C, D, E, F, G, A, B].choose(&mut thread_rng()).unwrap();
        let third = fundamental * 5.0 / 4.0;
        let fifth = fundamental * 3.0 / 2.0;
        let chord = vec![fundamental, fifth, third, fifth];

        let flute = &gen.instruments[0];
        let saw = &gen.instruments[1];
        let snare = &gen.instruments[2];
        let kick = &gen.instruments[3];

        let beat_length = gen.beat_length();
        let measure_length = beat_length * gen.time_signature as f32;

        eprintln!("New measure (length: {})", measure_length);

        for (beat, beat_timestamp) in gen.measure(gen.time_signature as usize) {
            gen.notes.push_back(Note {
                instrument: kick.clone(),
                start_timestamp: beat_timestamp,
                frequency: C / 4.0,
                duration: Second(1.0),
            });

            gen.notes.push_back(Note {
                instrument: snare.clone(),
                start_timestamp: beat_timestamp + beat_length * 0.5,
                frequency: C / 4.0,
                duration: Second(1.0),
            });

            gen.notes.push_back(Note {
                instrument: saw.clone(),
                start_timestamp: beat_timestamp,
                frequency: *chord.get(beat).unwrap_or(&fundamental),
                duration: beat_length * gen.time_signature as f32,
            });

            gen.notes.push_back(Note {
                instrument: flute.clone(),
                start_timestamp: beat_timestamp + beat_length * 0.75,
                frequency: fundamental * 3.0,
                duration: Second(4.0),
            });

            gen.notes.push_back(Note {
                instrument: flute.clone(),
                start_timestamp: beat_timestamp + beat_length * 0.25,
                frequency: fundamental * 2.0,
                duration: Second(4.0),
            });

            if beat_timestamp
                % (gen.time_signature / [1, 2, 2, 4, 4, 4].choose(&mut thread_rng()).unwrap())
                    as f32
                == Second(0.0)
            {
                gen.notes.push_back(Note {
                    instrument: flute.clone(),
                    start_timestamp: beat_timestamp + beat_length * 0.0,
                    frequency: [fundamental, third, fifth]
                        .choose(&mut thread_rng())
                        .unwrap()
                        * 2.0,
                    duration: Second(3.0),
                });
            }

            if beat_timestamp
                % (gen.time_signature / [1, 2, 2, 4, 4, 4].choose(&mut thread_rng()).unwrap())
                    as f32
                == Second(0.0)
            {
                gen.notes.push_back(Note {
                    instrument: flute.clone(),
                    start_timestamp: beat_timestamp + beat_length * 0.5,
                    frequency: [fundamental, third, fifth]
                        .choose(&mut thread_rng())
                        .unwrap()
                        * 1.0,
                    duration: Second(3.0),
                });
            }
        }

        gen.print_notes();
        measure_length
    }
}
