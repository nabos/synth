pub mod types;

use types::{Sample, Second};

pub fn print_sample(sample: Sample, min: Sample, max: Sample) {
    let width = 100.0;
    let sample_value = sample;
    let mut sample = sample;
    let mut min = min;
    let mut max = max;
    if min < Sample(0.0) {
        sample = sample - min;
        max = max - min;
        min = Sample(0.0);
    }
    if sample > max {
        sample = max;
    }
    if sample < min {
        sample = min;
    }
    let offset: f32 = (sample * width / (max - min)).into();
    let offset = offset as usize;
    eprintln!(
        "{:·>offset$}{:·>comp$} | {:.2}",
        "⬤",
        " ",
        sample_value,
        offset = offset,
        comp = width as usize * 2 - offset
    );
}

pub fn print_envelope(envelope: fn(Second, Second) -> Sample) {
    let length: f32 = 4.0; // in second
    let divider: f32 = 50.0; // resolution of a second
    eprintln!("------------------");
    for i in 0i32..(length * divider) as i32 {
        let i = Second(i as f32 / divider);
        eprint!("{:.2} ", i);
        print_sample(envelope(i, Second(1.0)), Sample(-1.0), Sample(1.0));
    }
    eprintln!("------------------");
}
