use crate::engine::instruments::*;

use std::sync::mpsc::{Receiver, Sender};
use std::sync::{mpsc, Arc};
use std::thread;

use std::collections::{HashMap, VecDeque};

use super::utils::types::{Frequency, Sample, Second};

#[derive(Debug)]
pub struct Note {
    pub instrument: Arc<Instrument>, // TODO we should be able to do that without Arc, but that means lot of lifetime headaches
    pub start_timestamp: Second,
    pub frequency: Frequency,
    pub duration: Second,
}

impl Note {
    fn get_sample(&self, current_timestamp: Second) -> Sample {
        if self.start_timestamp <= current_timestamp {
            let x = current_timestamp - self.start_timestamp; // Get current timestamp relative to note beginning
            self.instrument.get_sample(x, self.frequency, self.duration)
        } else {
            Sample(0.0)
        }
    }

    fn is_finished(&self, timestamps: &Vec<Second>) -> bool {
        let mut sample = Sample(0.0);
        for i in timestamps {
            sample = sample + self.get_sample(*i);
        }
        sample == Sample(0.0)
    }
}

pub struct SampleGenerator {
    sample_buffer: VecDeque<Sample>,
    buffer_tx_empty: Sender<VecDeque<Sample>>,
    buffer_rx_full: Receiver<VecDeque<Sample>>,
}

impl SampleGenerator {
    /// Generate next signal sample.
    pub fn next_sample(&mut self) -> Sample {
        if self.sample_buffer.is_empty() {
            self.sample_buffer = self.buffer_rx_full.recv().unwrap();
            self.buffer_tx_empty.send(VecDeque::new()).unwrap()
        }
        self.sample_buffer.pop_front().unwrap_or(Sample(0.0))
    }
}

pub trait MusicGenerator {
    fn new_generator(tempo: Option<u32>) -> Generator;
    fn gen_notes(gen: &mut Generator) -> Second;
}

pub struct Generator {
    sample_rate: u32,
    sample_clock: u32,
    time_between_seconds: Second,
    generation_current_timestamp: Second,
    pub seconds_counter: u32,
    pub tempo: u32,
    pub time_signature: u32,
    pub instruments: Vec<Arc<Instrument>>,
    pub notes: VecDeque<Note>,
    pub gen_notes: fn(&mut Generator) -> Second,
}

impl Generator {
    pub fn new_generator(
        sample_rate: u32,
        tempo: u32,
        instruments: Vec<Arc<Instrument>>,
    ) -> Generator {
        eprintln!("Setting tempo to {}", tempo);
        Generator {
            sample_rate,
            sample_clock: 0,
            time_between_seconds: Second(0.0),
            generation_current_timestamp: Second(0.0),
            seconds_counter: 0,
            tempo,
            time_signature: 4,
            instruments,
            notes: VecDeque::new(),
            gen_notes: |_| Second(0.0),
        }
    }

    pub fn print_notes(&self) {
        let beat_resolution = 8;
        let line_buffer_length = beat_resolution * self.time_signature;
        let measure_duration = self.time_signature as f32 * 60.0 / self.tempo as f32;

        let mut buffer: HashMap<&str, Vec<char>> = HashMap::new();
        for i in &self.instruments {
            buffer.insert(i.name, (0..line_buffer_length).map(|_| '-').collect());
        }

        for note in self.notes.iter() {
            if note.start_timestamp >= self.current_timestamp() {
                let offset: usize = ((note.start_timestamp.0 - self.generation_current_timestamp.0)
                    * line_buffer_length as f32
                    / measure_duration) as usize;
                buffer.get_mut(&note.instrument.name).unwrap()[offset] = 'X';
            }
        }
        for i in &self.instruments {
            eprintln!(
                "    |{}| {}",
                buffer.get(i.name).unwrap().iter().collect::<String>(),
                i.name
            );
        }
    }

    pub fn beat_length(&self) -> Second {
        Second(60.0 / self.tempo as f32)
    }

    pub fn measure(&self, length: usize) -> Vec<(usize, Second)> {
        let mut v = vec![];
        let beat_length = self.beat_length();
        for i in 0..length {
            let beat_timestamp = self.generation_current_timestamp + beat_length * i as f32;
            v.push((i, beat_timestamp));
        }
        v
    }

    fn current_timestamp(&self) -> Second {
        self.time_between_seconds + self.seconds_counter as f32
    }

    fn clock_tick(&mut self) {
        self.sample_clock = (self.sample_clock + 1) % self.sample_rate;
        self.time_between_seconds = Second(self.sample_clock as f32 / self.sample_rate as f32);
        if self.sample_clock == 0 {
            self.seconds_counter += 1;
        }
    }

    fn get_next_clock_ticks(&self, count: u32) -> Vec<Second> {
        let mut ticks = vec![];
        let mut seconds_counter = self.seconds_counter as f32;
        for i in 1..count {
            let sample_clock = (self.sample_clock + i) % self.sample_rate;
            let time_between_seconds = sample_clock as f32 / self.sample_rate as f32;
            if sample_clock == 0 {
                seconds_counter += 1.0;
            }
            ticks.push(Second(seconds_counter + time_between_seconds));
        }
        ticks
    }

    fn gen_buffers(
        &mut self,
        tx_full: Sender<VecDeque<Sample>>,
        rx_empty: Receiver<VecDeque<Sample>>,
    ) {
        loop {
            let mut buffer = rx_empty.recv().unwrap();
            buffer.clear();

            let measure_length = (self.gen_notes)(self);
            self.generation_current_timestamp = self.generation_current_timestamp + measure_length;

            while (buffer.len() as f32) < (measure_length * self.sample_rate as f32).into() {
                // This is needed to get current sample
                let current_timestamp = self.current_timestamp();
                // This is needed to determine if a note is finished
                // It boils down to knowing if the sum of next X sample is 0.0 or not
                let next_ts = self.get_next_clock_ticks(4);

                // This makes the generator clock tick once
                self.clock_tick();

                let mut sample = Sample(0.0);
                let mut notes = VecDeque::new();

                // All notes will be drained, not all of them will be pushed back
                for note in self.notes.drain(..) {
                    if note.start_timestamp <= current_timestamp {
                        sample = sample + note.get_sample(current_timestamp);

                        if !note.is_finished(&next_ts) {
                            notes.push_back(note);
                        } // else, note is dropped
                    } else {
                        // note hasn't started yet, keep it for later
                        notes.push_back(note);
                    }
                }
                buffer.push_back(sample);
                self.notes = notes;
            }
            eprintln!(
                "Queued {} samples (sample rate {})",
                buffer.len(),
                self.sample_rate
            );
            tx_full.send(buffer).unwrap();
        }
    }

    pub fn start_generation(mut self, sample_rate: u32) -> SampleGenerator {
        self.sample_rate = sample_rate;

        let (tx_full, rx_full) = mpsc::channel();
        let (tx_empty, rx_empty) = mpsc::channel();

        thread::spawn(move || {
            self.gen_buffers(tx_full, rx_empty);
        });

        tx_empty.send(VecDeque::new()).unwrap(); // Initialize the loop

        SampleGenerator {
            sample_buffer: VecDeque::new(),
            buffer_rx_full: rx_full,
            buffer_tx_empty: tx_empty,
        }
    }
}
