#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub struct Sample(pub f32);

impl std::fmt::Display for Sample {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::convert::From<f32> for Sample {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

impl std::convert::From<Sample> for f32 {
    fn from(value: Sample) -> Self {
        value.0
    }
}

impl<T> std::ops::Add<T> for Sample
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        Self(self.0 + f32::from(rhs))
    }
}

impl<T> std::ops::Sub<T> for Sample
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn sub(self, rhs: T) -> Self::Output {
        Self(self.0 - f32::from(rhs))
    }
}

impl<T> std::ops::Mul<T> for Sample
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self(self.0 * f32::from(rhs))
    }
}

impl std::ops::Mul<Sample> for f32 {
    type Output = Self;

    fn mul(self, rhs: Sample) -> Self::Output {
        self * f32::from(rhs)
    }
}

impl<T> std::ops::Div<T> for Sample
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Self(self.0 / f32::from(rhs))
    }
}

impl std::ops::Div<Sample> for f32 {
    type Output = Self;

    fn div(self, rhs: Sample) -> Self::Output {
        self / f32::from(rhs)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub struct Second(pub f32);

impl std::fmt::Display for Second {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::convert::From<f32> for Second {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

impl std::convert::From<Second> for f32 {
    fn from(value: Second) -> Self {
        value.0
    }
}

impl<T> std::ops::Add<T> for Second
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        Self(self.0 + f32::from(rhs))
    }
}

impl<T> std::ops::Sub<T> for Second
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn sub(self, rhs: T) -> Self::Output {
        Self(self.0 - f32::from(rhs))
    }
}

impl<T> std::ops::Mul<T> for Second
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self(self.0 * f32::from(rhs))
    }
}

impl<T> std::ops::Div<T> for Second
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Self(self.0 / f32::from(rhs))
    }
}

impl std::ops::Div<Second> for f32 {
    type Output = Self;

    fn div(self, rhs: Second) -> Self::Output {
        self / f32::from(rhs)
    }
}

impl<T> std::ops::Rem<T> for Second
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn rem(self, rhs: T) -> Self::Output {
        Self(self.0 % f32::from(rhs))
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Frequency(pub f32);

impl std::convert::From<f32> for Frequency {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

impl std::convert::From<Frequency> for f32 {
    fn from(value: Frequency) -> Self {
        value.0
    }
}

impl<T> std::ops::Add<T> for Frequency
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        Self(self.0 + f32::from(rhs))
    }
}

impl<T> std::ops::Mul<T> for Frequency
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self(self.0 * f32::from(rhs))
    }
}

impl<T> std::ops::Div<T> for Frequency
where
    f32: From<T>,
    T: Copy,
{
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Self(self.0 / f32::from(rhs))
    }
}

impl<T> std::ops::Mul<T> for &Frequency
where
    f32: From<T>,
    T: Copy,
{
    type Output = Frequency;

    fn mul(self, rhs: T) -> Self::Output {
        Frequency(self.0 * f32::from(rhs))
    }
}
