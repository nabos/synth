use super::utils::types::Frequency;

pub const C: Frequency = Frequency(261.6256);
pub const D: Frequency = Frequency(293.6648);
pub const E: Frequency = Frequency(329.6276);
pub const F: Frequency = Frequency(349.2282);
pub const G: Frequency = Frequency(391.9954);
pub const A: Frequency = Frequency(440.0);
pub const B: Frequency = Frequency(493.8833);
