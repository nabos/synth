/// Signal Generator.
/// This comes from https://github.com/klangner/sounds
use core::f32::consts::PI;
use rand::{thread_rng, Rng};

use super::utils::types::{Frequency, Sample, Second};

pub fn noise() -> Sample {
    Sample(thread_rng().gen::<f32>())
}

pub fn sine(i: f32, freq: Frequency) -> Sample {
    let w: f32 = (freq * 2.0 * PI).into();
    Sample((i * w).sin())
}

pub fn triangle(i: f32, freq: Frequency) -> Sample {
    let w: f32 = (freq * 4.0).into();
    let t = i * w % 4.0;
    if t <= 2.0 {
        Sample(t - 1.0)
    } else {
        Sample(-t + 3.0)
    }
}

pub fn square(i: f32, freq: Frequency) -> Sample {
    let w: f32 = (freq * 2.0).into();
    if (i * w) % 2.0 >= 1.0 {
        Sample(1.0)
    } else {
        Sample(-1.0)
    }
}

pub fn sawtooth(i: f32, freq: Frequency) -> Sample {
    let w: f32 = (freq * 2.0).into();
    Sample(-((i * w) % 2.0 - 1.0))
}

pub fn envelope(
    x: Second, // x here represents the current progression of the note, starting at 0, and counting the time in second
    duration: Second,
    attack_length: Second,
    decay_length: Second,
    sustain_strength: f32,
    release_length: Second,
) -> Sample {
    if x < duration && x < attack_length {
        // println!("attack");
        let x = x;
        Sample((x / attack_length).into())
    } else if x < duration && x < attack_length + decay_length {
        // println!("decay");
        let top_height = 1.0;
        let x: f32 = (x - attack_length).into();
        Sample(top_height - x * (top_height - sustain_strength) / decay_length)
    } else if x < duration {
        // println!("sustain");
        Sample(sustain_strength)
    } else if x >= duration && x < duration + release_length {
        // println!("release");
        let x: f32 = (x - duration).into();
        Sample(sustain_strength - x * sustain_strength / release_length)
    } else {
        Sample(0.0)
    }
}

#[derive(Debug)]
pub struct Sound {
    pub volume: u8, // range: [0,100] - the general volume of the sound
    pub wave: fn(f32, Frequency) -> Sample, // sine, triangle, sawtooth, etc...
    pub pitch_envelope: fn(Second, Second) -> Frequency, // how the pitch will vary (]∞, ∞[, result will be added to the note frequency)
    pub amplitude_envelope: fn(Second, Second) -> Sample, // how the amplitude will vary ([0, 1], result will be multiplied to the signal)
    pub noise: u8, // [0,100] - how much noise do we add to the sound
}

impl Sound {
    pub fn get_sample(
        &self,
        x: Second, // x here represents the current progression of the note, starting at 0, and counting the time in second
        frequency: Frequency,
        duration: Second,
    ) -> Sample {
        // if x == Second(0.0) {
        //     super::utils::print_envelope(self.amplitude_envelope);
        // }
        let freq = frequency + (self.pitch_envelope)(x, duration);

        let mut new_sample = (self.wave)(x.into(), freq);

        if self.noise > 0 {
            new_sample = new_sample + noise() * self.noise as f32 / 100.0;
        }

        new_sample = new_sample * (self.amplitude_envelope)(x, duration);

        new_sample * self.volume as f32 / 100.0
    }
}
