use crate::engine::sounds::*;

use super::utils::types::{Frequency, Sample, Second};

#[derive(Debug)]
pub struct Instrument {
    pub name: &'static str,
    sounds: Vec<Sound>,
}

impl Instrument {
    pub fn new_tone(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 100,
                wave: sine,
                pitch_envelope: |_, _| Frequency(0.0),
                amplitude_envelope: |x, duration| {
                    envelope(x, duration, Second(0.0), Second(0.0), 1.0, Second(0.0))
                },
                noise: 0,
            }],
        }
    }

    pub fn new_kick(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![
                Sound {
                    volume: 100,
                    wave: sine,
                    pitch_envelope: |_, _| Frequency(0.0),
                    amplitude_envelope: |x, _| {
                        envelope(x, Second(0.5), Second(0.001), Second(0.1), 0.0, Second(0.0))
                    },
                    noise: 0,
                },
                Sound {
                    volume: 100,
                    wave: triangle,
                    pitch_envelope: |_, _| Frequency(0.0),
                    amplitude_envelope: |x, _| {
                        envelope(x, Second(0.5), Second(0.001), Second(0.1), 0.0, Second(0.0))
                    },
                    noise: 0,
                },
            ],
        }
    }

    pub fn new_snare(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 100,
                wave: triangle,
                pitch_envelope: |x, _| {
                    Frequency(
                        300.0
                            * envelope(
                                x,
                                Second(0.5),
                                Second(0.001),
                                Second(0.11),
                                0.0,
                                Second(0.0),
                            ),
                    )
                },
                amplitude_envelope: |x, _| {
                    envelope(x, Second(0.5), Second(0.001), Second(0.1), 0.0, Second(0.0))
                },
                noise: 50,
            }],
        }
    }

    pub fn new_crash(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 40,
                wave: |i, freq| sine(i, freq * 0.5) * 0.3 * i + 0.6 * noise(),
                pitch_envelope: |x, duration| {
                    let rest = duration - x + Second(4.0);
                    Frequency((rest * 100.0).into())
                },
                amplitude_envelope: |x, duration| {
                    envelope(
                        x,             // sample
                        duration,      // note duration,
                        Second(0.001), // attack length
                        Second(0.7),   // decay length
                        0.2,           // sustain strenth
                        Second(1.0),   // release length
                    )
                },
                noise: 0,
            }],
        }
    }

    pub fn new_bell(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 20,
                wave: |i, freq| {
                    let rest = 5.0 - i;
                    sine(i, freq)
                        + sine(i, freq * 1.5) * rest * rest / 10.0
                        + sine(i, freq * 3.0) * rest * rest / 10.0
                        + sine(i, freq * 0.5) * i
                        + 0.1 * noise()
                },
                pitch_envelope: |_, _| Frequency(0.0),
                amplitude_envelope: |x, duration| {
                    envelope(
                        x,             // sample
                        duration,      // note duration,
                        Second(0.001), // attack length
                        Second(0.7),   // decay length
                        0.2,           // sustain strenth
                        Second(3.0),   // release length
                    )
                },
                noise: 0,
            }],
        }
    }

    pub fn new_flute(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 10,
                wave: |i, freq| {
                    sine(i, freq) + sine(i, freq * 0.75) * (sine(i, Frequency(0.4)) + 2.0) * 0.5
                },
                pitch_envelope: |_, _| Frequency(0.0),
                amplitude_envelope: |x, duration| {
                    envelope(
                        x,           // sample
                        duration,    // note duration,
                        Second(0.1), // attack length
                        Second(0.7), // decay length
                        0.2,         // sustain strenth
                        Second(3.0), // release length
                    )
                },
                noise: 0,
            }],
        }
    }

    pub fn new_supersaw(name: &'static str) -> Self {
        Instrument {
            name,
            sounds: vec![Sound {
                volume: 2,
                wave: |i, freq| {
                    sawtooth(i, freq)
                        + sawtooth(i, freq * 0.75) * (sine(i, Frequency(0.4)) + 2.0) * 0.5
                        + sawtooth(i, freq * 0.5) * (sine(i, Frequency(0.3)) + 2.0) * 0.5
                        + sawtooth(i, freq * 0.25) * (sine(i, Frequency(0.5)) + 2.0) * 0.5
                },
                pitch_envelope: |_, _| Frequency(0.0),
                amplitude_envelope: |x, duration| {
                    envelope(
                        x,           // sample
                        duration,    // note duration,
                        Second(0.1), // attack length
                        Second(0.7), // decay length
                        0.2,         // sustain strenth
                        Second(3.0), // release length
                    )
                },
                noise: 0,
            }],
        }
    }

    pub fn get_sample(
        &self,
        x: Second, // x here represents the current progression of the note, starting at 0, and counting the time in second
        frequency: Frequency,
        duration: Second,
    ) -> Sample {
        let mut sample = Sample(0.0);
        for sound in &self.sounds {
            sample = sample + sound.get_sample(x, frequency, duration);
        }
        sample
    }
}
