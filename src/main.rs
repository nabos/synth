mod engine;
mod lofi_generator;
mod test_generator;

use std::io::{stdout, Write};

use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::StreamConfig;

use engine::generator::Generator;

use crate::{
    engine::generator::MusicGenerator, lofi_generator::LofiGenerator, test_generator::TestGenerator,
};

use rargsxd::{Arg, ArgParser};

fn play_cpal(generator: Generator) {
    let host = cpal::default_host();
    let device = host
        .default_output_device()
        .expect("no output device available");

    let mut supported_configs_range = device
        .supported_output_configs()
        .expect("error while querying configs");
    let supported_config = supported_configs_range
        .next()
        .expect("no supported config?!")
        .with_sample_rate(cpal::SampleRate(44100)); // CD quality will be sufficient

    let config: StreamConfig = supported_config.into();

    let mut sample_generator = generator.start_generation(config.sample_rate.0);

    let stream = device
        .build_output_stream(
            &config,
            move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
                for frame in data.chunks_mut(config.channels.into()) {
                    for sample in frame.iter_mut() {
                        *sample = sample_generator.next_sample().into();
                    }
                }
                // react to stream events and read or write stream data here.
            },
            move |_| {
                // react to errors here.
            },
        )
        .unwrap();

    stream.play().unwrap();

    loop {
        std::thread::sleep(std::time::Duration::from_millis(10000));
    }
}

fn play_wave(generator: Generator) {
    let mut sample_generator = generator.start_generation(44100);

    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };
    let mut writer = hound::WavWriter::create("synth.wav", spec).unwrap();
    let mut i = 44100;
    loop {
        let sample: f32 = sample_generator.next_sample().into();
        let amplitude = i16::MAX as f32;
        writer.write_sample((sample * amplitude) as i16).unwrap();
        i -= 1;
        if i == 0 {
            writer.flush().unwrap();
            i = 44100;
        }
    }
}

fn play_pcm(generator: Generator) {
    let mut sample_generator = generator.start_generation(44100);

    let mut stdout = std::io::stdout().lock();

    loop {
        let sample: f32 = sample_generator.next_sample().into();
        let amplitude = i16::MAX as f32;
        stdout
            .write_all(&((sample * amplitude) as i16).to_le_bytes())
            .unwrap();
    }
}

fn main() {
    let mut args = ArgParser::new("synth");
    args.info("A simple music generator")
        .author("Skia")
        // .version("0.1.0")
        // .copyright("Copyright (C) 2023 Skia")
        .usage(
            "{name} [OPTIONS]

Backends:
    cpal: will play the sound on your default audio output
    wave: will produce a synth.wave file with the music
    pcm: will output a 16 bit little endian, 44100Hz, mono PCM
         Can be used like that:
         {name} -b pcm | aplay -f cd -c 1
",
        )
        .args(vec![
            Arg::str("backend", "cpal")
                .short('b')
                .long("backend")
                .help("[default: cpal] [possible values: cpal, wave, pcm]"),
            Arg::str("tempo", "")
                .short('t')
                .long("tempo")
                .help("Tempo of the music"),
            Arg::str("generator", "")
                .short('g')
                .long("generator")
                .help("[default: lofi] [possible values: lofi, test]"),
        ])
        .parse();

    let tempo: Option<u32> = match args.get_str("tempo").as_str().trim() {
        "" => None,
        tempo => tempo.parse().ok(),
    };
    let generator = args.get_str("generator");

    let music_generator = match generator.as_str().trim() {
        "test" => TestGenerator::new_generator(tempo),
        _ => LofiGenerator::new_generator(tempo),
    };

    match args.get_str("backend").as_str().trim() {
        "pcm" => play_pcm(music_generator),
        "wave" => play_wave(music_generator),
        _ => play_cpal(music_generator),
    };
}
